var keysPressed = [];
var displayKeys = [];
var pressedKeyContainer = document.getElementById("pressedKeyContainer");
var isMenuDisplayed = false;

// List of shortcuts available
const controls = [
  {
    control: "ControlLeft" + "KeyF",
    callback: shortCutSearch,
  },
  {
    control: "ControlLeft" + "KeyC",
    callback: shortCutCopy,
  },
  {
    control: "ControlLeft" + "KeyP",
    callback: sendToPrinter,
  },
];

document.addEventListener("keydown", function (e) {
  e.preventDefault();
  e.stopPropagation();

  let keyExist = keysPressed.find((key) => key.code == e.code);
  if (!keyExist) {
    let key = {
      code: e.code,
      key: e.key,
    };
    keysPressed.push(key);

    keysPressed.forEach((key) => {
      try {
        let divExist = pressedKeyContainer.querySelector(`#${key.code}`);
        if (!divExist) {
          let keyDiv = document.createElement("div");
          keyDiv.setAttribute("class", "pressedKey");
          keyDiv.setAttribute("id", e.code);
          keyDiv.innerHTML = e.key;

          pressedKeyContainer.appendChild(keyDiv);
        }
      } catch (error) {}
    });
  }
  isShortCut();
});

document.addEventListener("keyup", function (e) {
  try {
    hideShortcutsPopup(`#${e.code}`);

    let keyExist = keysPressed.find((key) => key.code == e.code);
    if (keyExist) {
      let removedKey = keysPressed.indexOf(keyExist);
      keysPressed.splice(removedKey);
    }
  } catch (error) {}
});

function hideShortcutsPopup(divNameOrId) {
  divToRemove = pressedKeyContainer.querySelectorAll(divNameOrId);
  divToRemove.forEach((div) => {
    pressedKeyContainer.removeChild(div);
  });
}

function isShortCut() {
  pressedKeyContainer.style.backgroundColor = "rgba(85, 85, 85, 0.5)";
  let fullControll = "";
  keysPressed.forEach((key) => {
    fullControll = fullControll + key.code;
  });

  controls.forEach((control) => {
    if (control.control == fullControll) {
      pressedKeyContainer.style.backgroundColor = "rgba(51, 179, 0, 0.5)";
      control.callback();
    }
  });
}

function shortCutSearch() {
  console.log("Search shortcut");
}

function shortCutCopy() {
  console.log("copied", window.getSelection().toString());
  navigator.clipboard.writeText(document.getSelection().toString());
}

document.addEventListener("click", (e) => {
  hideDisplayedContextMenu();
});

// Right click menu display options switch
let allContents = document.querySelectorAll("body");
allContents.forEach((item) => {
  item.addEventListener("contextmenu", (e) => {
    switch (e.target.nodeName) {
      case "IMG":
        hideDisplayedContextMenu();
        displayMenuOnRightClickImage(e);
        break;
      case "DIV":
      case "H1":
      case "H2":
      case "H3":
      case "H4":
      case "H5":
      case "H6":
      case "SPAN":
      case "UL":
      case "LI":
        hideDisplayedContextMenu();
        displayMenuOnRightClickOnOther(e);
        break;

      default:
        hideDisplayedContextMenu();
        break;
    }
  });
});

function hideDisplayedContextMenu() {
  if (isMenuDisplayed) {
    let menu = document.getElementById("context-menu");

    document.body.removeChild(menu);
    isMenuDisplayed = false;
  }
}

function setContextMenuPositionOnScreen(menu, event) {
  menu.style.top = `${event.pageY}px`;

  if (window.innerHeight / 2 > event.pageY) {
    menu.style.top = `${event.pageY}px`;
  } else {
    menu.style.top = `${event.pageY - menu.offsetHeight}px`;
  }

  if (window.innerWidth / 2 > event.clientX) {
    menu.style.left = `${event.clientX}px`;
  } else {
    menu.style.left = `${event.clientX - menu.offsetWidth}px`;
  }
}

function displayMenuOnRightClickImage(event) {
  event.preventDefault();
  let menu = document.createElement("ul");
  menu.setAttribute("class", "context-menu");
  menu.setAttribute("id", "context-menu");

  let menuItem1 = document.createElement("li");
  menuItem1.innerHTML = "Enregistrer l'image";
  menuItem1.setAttribute("class", "context-menu-item");
  menu.setAttribute("id", "context-menu");

  let menuItem2 = document.createElement("li");
  menuItem2.innerHTML = "Imprimer l'image";
  menuItem2.setAttribute("class", "context-menu-item");

  let menuItem3 = document.createElement("li");
  menuItem3.innerHTML = "menu Item";
  menuItem3.setAttribute("class", "context-menu-item");

  let menuItem4 = document.createElement("li");
  menuItem4.innerHTML = "menu Item";
  menuItem4.setAttribute("class", "context-menu-item");

  menu.appendChild(menuItem1);
  menu.appendChild(menuItem2);

  document.body.appendChild(menu);
  setContextMenuPositionOnScreen(menu, event);
  isMenuDisplayed = true;
}

function displayMenuOnRightClickOnOther(event) {
  event.preventDefault();
  // event.stopPropagation();
  let menu = document.createElement("ul");
  menu.setAttribute("class", "context-menu");
  menu.setAttribute("id", "context-menu");

  let menuItem1 = document.createElement("li");
  menuItem1.innerHTML = "Copier la selection";
  menuItem1.setAttribute("class", "context-menu-item");
  menuItem1.setAttribute("onclick", "shortCutCopy()");
  // menuItem1.onclick = shortCutCopy();

  let menuItem2 = document.createElement("li");
  menuItem2.innerHTML = "Imprimer la page";
  menuItem2.setAttribute("class", "context-menu-item");
  menuItem2.setAttribute("onclick", "sendToPrinter()");

  let menuItem3 = document.createElement("li");
  menuItem3.innerHTML = "menu Item";
  menuItem3.setAttribute("class", "context-menu-item");

  let menuItem4 = document.createElement("li");
  menuItem4.innerHTML = "menu Item";
  menuItem4.setAttribute("class", "context-menu-item");

  menu.appendChild(menuItem1);
  menu.appendChild(menuItem2);
  menu.appendChild(menuItem3);

  document.body.appendChild(menu);
  setContextMenuPositionOnScreen(menu, event);
  isMenuDisplayed = true;
}

function sendToPrinter() {
  hideDisplayedContextMenu();
  setTimeout(() => {
    hideShortcutsPopup("div");
  }, 100);
  setTimeout(() => {
    window.print();
  }, 100);
}
